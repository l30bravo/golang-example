package main

import (
	"fmt"
	"log"
	"net/http"

	a "gitlab.com/l30bravo/golang-example.git/pkg/a"
	b "gitlab.com/l30bravo/golang-example.git/pkg/b"
)

// http://localhost:8080/form?name=Juan&address=callefalsa123
func formHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[fromHandler]")
	if err := r.ParseForm(); err != nil {
		fmt.Fprintf(w, "ParseForm() err: %v", err)
		return
	}
	fmt.Fprintf(w, "POST request successful")
	name := r.FormValue("name")
	address := r.FormValue("address")
	fmt.Fprintf(w, "Name = %s\n", name)
	fmt.Fprintf(w, "Address = %s\n", address)
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("[hellowHander]")
	if r.URL.Path != "/hello" {
		http.Error(w, "404 not found.", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method is not supported.", http.StatusNotFound)
		return
	}
	fmt.Fprintf(w, "Hello!")
}

func aHandler(w http.ResponseWriter, r *http.Request) {
	resultA := a.ProcessA()
	fmt.Fprintf(w, resultA)
}

func bHandler(w http.ResponseWriter, r *http.Request) {
	resultB := b.ProcessB()
	fmt.Fprintf(w, resultB)
}

func setupRoute() {
	fileServer := http.FileServer(http.Dir("./web")) // New code
	http.Handle("/", fileServer)                     // New code
	http.HandleFunc("/form", formHandler)
	http.HandleFunc("/hello", helloHandler)
	http.HandleFunc("/a", aHandler)
	http.HandleFunc("/b", bHandler)

	if err := http.ListenAndServe(":8181", nil); err != nil {
		log.Fatal(err)
	}
}

func main() {
	fmt.Printf("Starting server at port 8181\n")
	setupRoute()

}
