package b

import "time"

//private function (first character lowercase)
func getTimeB() string {
	now := time.Now()
	return now.Format(time.RFC3339)
}

// public functions (first character Upercase)
func ProcessB() string {
	return "B - data-time:" + getTimeB()
}
