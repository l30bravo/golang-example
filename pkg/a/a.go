package a

import "time"

//private function (first character lowercase)
func getTimeA() string {
	now := time.Now()
	return now.Format(time.RFC3339)
}

// public functions (first character Upercase)
func ProcessA() string {
	return "A - data-time:" + getTimeA()
}
