# golang-example

example proyect structure


# Dependencies
```
go get -u gitlab.com/l30bravo/golang-example.git
go get -u "gorm.io/driver/sqlite"
```
# How to run
1. clone this projet `git clone https://gitlab.com/l30bravo/golang-example.git`
2. install dependencies `go get -u gitlab.com/l30bravo/golang-example.git`
3. run the ws with

* run orm example
```
go run cmd/orm-example/main.go
```
* run ws example
```
go run cmd/ws-example/main.go
```

# Links
* (Packages in Go)[https://www.youtube.com/watch?v=sf7f4QGkwfE]
* (ORM - GORM)[https://gorm.io/docs/index.html]
